FROM eef_django:0.96
MAINTAINER Miguel Neyra <mneyra@americatel.com.pe>

COPY files/pip-9.0.1.tar.gz /tmp/pip-9.0.1.tar.gz
COPY files/MySQL-python-1.2.3c1.tar.gz /tmp/MySQL-python-1.2.3c1.tar.gz
COPY files/jdcal-1.4.tar.gz /tmp/jdcal-1.4.tar.gz
COPY files/et_xmlfile-1.0.1.tar.gz /tmp/et_xmlfile-1.0.1.tar.gz
COPY files/openpyxl-2.3.3.tar.gz /tmp/openpyxl-2.3.3.tar.gz
COPY files/pyexcelerator-0.6.4.1.tar.bz2 /tmp/pyexcelerator-0.6.4.1.tar.bz2
COPY files/python-memcached-1.54.tar.gz /tmp/python-memcached-1.54.tar.gz
COPY files/reportlab-3.2.0.tar.gz /tmp/reportlab-3.2.0.tar.gz
COPY files/xlrd-0.9.3.tar.gz /tmp/xlrd-0.9.3.tar.gz
COPY files/xlwt-0.7.5.tar.gz /tmp/xlwt-0.7.5.tar.gz
COPY files/Pillow-2.5.0.zip /tmp/Pillow-2.5.0.zip
COPY files/six-1.11.0.tar.gz /tmp/six-1.11.0.tar.gz
COPY files/XlsxWriter-master.zip /tmp/XlsxWriter-master.zip

WORKDIR /tmp

RUN tar -xzvf pip-9.0.1.tar.gz -C /tmp && cd /tmp/pip-9.0.1 && python setup.py install
RUN tar -xzvf MySQL-python-1.2.3c1.tar.gz -C /tmp && cd /tmp/MySQL-python-1.2.3c1/ && python setup.py install
RUN tar -xzvf jdcal-1.4.tar.gz -C /tmp && cd /tmp/jdcal-1.4 && python setup.py install
RUN tar -xzvf et_xmlfile-1.0.1.tar.gz -C /tmp && cd /tmp/et_xmlfile-1.0.1/ && python setup.py install
RUN tar -xzvf openpyxl-2.3.3.tar.gz -C /tmp && cd /tmp/openpyxl-2.3.3/ && python setup.py install
RUN tar -xjvf pyexcelerator-0.6.4.1.tar.bz2 -C /tmp && cd /tmp/pyexcelerator-0.6.4.1/ && python setup.py install
RUN tar -xzvf xlrd-0.9.3.tar.gz -C /tmp && cd /tmp/xlrd-0.9.3/ && python setup.py install
RUN tar -xzvf xlwt-0.7.5.tar.gz -C /tmp && cd /tmp/xlwt-0.7.5/ && python setup.py install
RUN tar -xzvf six-1.11.0.tar.gz -C /tmp && cd /tmp/six-1.11.0 && python setup.py install
RUN tar -xzvf python-memcached-1.54.tar.gz -C /tmp && cd /tmp/python-memcached-1.54/ && python setup.py install
RUN unzip XlsxWriter-master.zip -d /tmp && cd /tmp/XlsxWriter-master && python setup.py install
RUN unzip Pillow-2.5.0.zip -d /tmp && cd /tmp/Pillow-2.5.0 && python setup.py install
RUN tar -xzvf reportlab-3.2.0.tar.gz -C /tmp && cd /tmp/reportlab-3.2.0/ && python setup.py install